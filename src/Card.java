

public class Card{

    String strCardDescription;

    //Card Constructor
    public Card(String strCardNotation)
    {
        strCardDescription = strCardNotation;
    }

    public void getDescription()
    {
        /*
        Switch statments to check for card notation with unknown being the default if no
        proper card notation is found
         */
        switch(strCardDescription) {
            case "AD":
                System.out.println("Ace of Diamonds");

                break;
            case "AH":
                System.out.println("Ace of Hearts");

                break;
            case "AS":
                System.out.println("Ace of Spades");

                break;
            case "AC":
                System.out.println("Ace of Clubs");

                break;
            case "2D":
                System.out.println("2 of Diamonds");

                break;
            case "2H":
                System.out.println("2 of Hearts");

                break;
            case "2S":
                System.out.println("2 of Spades");

                break;
            case "2C":
                System.out.println("2 of Clubs");

                break;
            case "3D":
                System.out.println("3 of Diamonds");

                break;
            case "3H":
                System.out.println("3 of Hearts");

                break;
            case "3S":
                System.out.println("3 of Spades");

                break;
            case "3C":
                System.out.println("3 of Clubs");

                break;
            case "4D":
                System.out.println("4 of Diamonds");

                break;
            case "4H":
                System.out.println("4 of Hearts");

                break;
            case "4S":
                System.out.println("4 of Spades");

                break;
            case "4C":
                System.out.println("4 of CLubs");

                break;
            case "5D":
                System.out.println("5 of Diamonds");

                break;
            case "5H":
                System.out.println("5 of Hearts");

                break;
            case "5S":
                System.out.println("5 of Spades");

                break;
            case "5C":
                System.out.println("5 of Clubs");

                break;
            case "6D":
                System.out.println("6 of Diamonds");

                break;
            case "6H":
                System.out.println("6 of Hearts");

                break;
            case "6S":
                System.out.println("6 of Spades");

                break;
            case "6C":
                System.out.println("6 of Clubs");

                break;
            case "7D":
                System.out.println("7 of Diamonds");

                break;
            case "7H":
                System.out.println("7 of Hearts");

                break;
            case "7S":
                System.out.println("7 of Spades");

                break;
            case "7C":
                System.out.println("7 of Clubs");

                break;
            case "8D":
                System.out.println("8 of Diamonds");

                break;
            case "8H":
                System.out.println("8 of Hearts");

                break;
            case "8S":
                System.out.println("8 of Spades");

                break;
            case "8C":
                System.out.println("8 of Clubs");

                break;
            case "9D":
                System.out.println("9 of Diamonds");

                break;
            case "9H":
                System.out.println("9 of Hearts");

                break;
            case "9S":
                System.out.println("9 of Spades");

                break;
            case "9C":
                System.out.println("9 of Clubs");

                break;
            case "10D":
                System.out.println("10 of Diamonds");

                break;
            case "10H":
                System.out.println("10 of Hearts");

                break;
            case "10S":
                System.out.println("10 of Spades");

                break;
            case "10C":
                System.out.println("10 of CLubs");

                break;
            case "JD":
                System.out.println("Jack of Diamonds");

                break;
            case "JH":
                System.out.println("Jack of Hearts");

                break;
            case "JS":
                System.out.println("Jack of Spades");

                break;
            case "JC":
                System.out.println("Jack of Clubs");

                break;
            case "QD":
                System.out.println("Queen of Diamonds");

                break;
            case "QH":
                System.out.println("Queen of hearts");

                break;
            case "QS":
                System.out.println("Queen of Spades");

                break;
            case "QC":
                System.out.println("Queen of Clubs");

                break;
            case "KD":
                System.out.println("King of Diamonds");

                break;
            case "KH":
                System.out.println("King of Hearts");

                break;
            case "KS":
                System.out.println("King of Spades");

                break;
            case "KC":
                System.out.println("King of Clubs");

                break;

            default:
                System.out.println("unknown");
        }
    }
}

