import java.util.Scanner;

/*
* Austin Rafter
* 013433901
* CS 049J
* 9/11/2020
*
* Take user input and output card notation if correctly entered
* Print unknown if incorrect format
 */
public class E5_19 {
    public static void main(String[] args) {
        System.out.println("Enter the Card Notation:");

        Scanner scanObject = new Scanner(System.in); //create scanner object to get user input

        String strCardNotationEntry = scanObject.nextLine(); //user input

        String strCardNotationToUpperCase = strCardNotationEntry.toUpperCase(); //set entry to uppercase
        //to upper case handles lower case input in correct card notation formatting

        Card userCardNotation = new Card(strCardNotationToUpperCase); //initiate new Card object

        userCardNotation.getDescription();  //call getDescription method on the object
    }



}

